import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Footer from "./components/footer";
import Navbar from "./components/navbar";
import { AppProvider } from "./context/AppContext";
import ChangePasswordForm from "./pages/ChangePasswordForm";
import GameForm from "./pages/GameForm";
import GameListView from "./pages/GameList";
import GameTable from "./pages/GameTable";
import Home from "./pages/Home";
import Login from "./pages/Login";
import MovieForm from "./pages/MovieForm";
import MovieListView from "./pages/MovieList";
import MovieTable from "./pages/MovieTable";
import Register from "./pages/Register";
import ViewGame from "./pages/ViewGame";
import ViewMovie from "./pages/ViewMovie";

export default function Routes(){
    return (
        <>
            <Router>
                <AppProvider>
                    <Navbar />
                    <Switch>
                        <Route path="/" exact>
                            <Home/>
                        </Route>

                        <Route path="/movies" exact component={MovieListView} />
                        <Route path="/movie/all" exact component={MovieTable} />
                        <Route path="/movie/add" exact component={MovieForm} />
                        <Route path="/movie/edit/:ID" exact component={MovieForm} />
                        <Route path="/movie/:ID" exact component={ViewMovie} />

                        <Route path="/games" exact component={GameListView} />
                        <Route path="/game/all" exact component={GameTable} />
                        <Route path="/game/add" exact component={GameForm} />
                        <Route path="/game/edit/:ID" exact component={GameForm} />
                        <Route path="/game/:ID" exact component={ViewGame} />

                        <Route path="/login" exact component={Login} />
                        <Route path="/register" exact component={Register} />
                        <Route path="/changepassword" exact component={ChangePasswordForm} />

                    </Switch>
                    <Footer/>
                </AppProvider>
            </Router>
        </>
    );
}