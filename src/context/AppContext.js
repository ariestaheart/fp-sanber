import axios from "axios";
import React, { createContext, useContext, useState } from "react";
import { useHistory } from "react-router";
import { message } from "antd";
import Cookies from "js-cookie"
import { Redirect } from "react-router-dom";

export const AppContext = createContext();

export const AppProvider = (props) => {
    let history = useHistory();

    const [searchVal, setSearchVal] = useState('');

    const [movieList, setMovieList] = useState([]);
    const [gameList, setGameList] = useState([]);

    const checkAuth = () => {
        console.log(Cookies.get('token'));
        if(Cookies.get('token') == undefined ) {
            window.location = "/login";
        }
    }

    const [inputMovie, setInputMovie] = useState({
        created_at: "",
        description: "",
        duration: 0,
        genre: "",
        id: -1,
        image_url: "",
        rating: 0,
        review: "",
        title: "",
        updated_at: "",
        year: 1980,
    });

    // MOVIE
    const getMovieList = async () => {
        const result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`);        
        setMovieList(result.data.map((res, index)=>{
            return {
                ...res, number : index + 1
            }
        }));
    }

    const getMovieByID = async (id) => {
        // https://backendexample.sanbersy.com/api/data-movie/
        const result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie/${id}`);        
        console.log(result.data);
        setInputMovie(result.data);
        return result.data;
    }

    // GAME
    const [inputGame, setInputGame] = useState({
        created_at: "",
        genre: "",
        id: -1,
        image_url: "",
        multiplayer: 1,
        singlePlayer: 1,
        name: "",
        platform: "",
        release: 2021,
        updated_at: "",
    });

    const getGameList = async () => {
        const result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`);        
        setGameList(result.data.map((res, index)=>{
            return {
                ...res, number : index + 1
            }
        }));
    }

    const getGameByID = async (id) => {
        // https://backendexample.sanbersy.com/api/data-movie/
        const result = await axios.get(`https://backendexample.sanbersy.com/api/data-game/${id}`);        
        console.log(result.data);
        setInputGame(result.data);
        return result.data;
    }

    // const getAppById = async (id) => {
    //     const result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${id}`);        
    //     setInput(result.data);
    // }

    return (
        <AppContext.Provider value={{
            history,
            movieList, setMovieList,
            gameList, setGameList,
            searchVal, setSearchVal,
            inputMovie, setInputMovie,
            inputGame, setInputGame,
            getGameList,
            getMovieList,
            getMovieByID,
            getGameByID,
            checkAuth
        }}>
            {props.children}
        </AppContext.Provider>
    );
}
