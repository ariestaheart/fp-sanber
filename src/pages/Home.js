import { Col, Row, Layout, Menu, Breadcrumb } from "antd";
import React, { useContext, useEffect, useState } from "react";
import CardComponent from "../components/card";
import CardApp from "../components/cardApp";
import Footer from "../components/footer";
import Navbar from "../components/navbar";
import Section from "../components/section";
import { AppContext } from "../context/AppContext";
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import Cookies from "js-cookie"
import SideBar from "../components/sidebar";


const { SubMenu } = Menu;
const { Sider } = Layout;

export default function Home(){
    const {
        user,
        history,
        movieList,
        gameList,
        getGameList,
        getMovieList
    } = useContext(AppContext);

    const [first, setFirst] = useState(false);

    useEffect(() => {
        console.log("effect home");
        if(!first){
            getGameList();
            getMovieList();
            setFirst(true);    
        }
    }, []);

    return (
		<>
            <div style={{display:"flex"}}>
                <SideBar/>
                <Section>
                    <div style={{textAlign:"center", marginBottom:"2rem"}}>
                        <h1>Movies List</h1>
                    </div>
                    <Row gutter={16}>
                        {movieList.map((data, index) => {
                            return(
                                <Col key={index} onClick={() => history.push(`/movie/${data.id}`)} span={6} style={{marginBottom:"4rem"}}>
                                    <CardComponent data={data} type="movie"></CardComponent>
                                </Col>
                            );
                        })}
                    </Row>
                    <hr/>
                    <div style={{textAlign:"center", marginBottom:"2rem"}}>
                        <h1>Games List</h1>
                    </div>
                    <Row gutter={16}>
                        {gameList.map((data, index) => {
                            return(
                                <Col key={index} onClick={() => history.push(`/game/${data.id}`)} span={6} style={{marginBottom:"4rem"}}>
                                    <CardComponent data={data} type="game"></CardComponent>
                                </Col>
                            );
                        })}
                    </Row>
                </Section>
            </div>
		</>
    );
}