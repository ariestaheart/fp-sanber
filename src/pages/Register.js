import { Card, Col, Row, Form, Input, Button, Checkbox } from "antd";
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons';

import React, { useContext, useEffect, useState } from "react";
import CardComponent from "../components/card";
import Section from "../components/section";
import { AppContext } from "../context/AppContext";
import { Link } from "react-router-dom";
import { message } from "antd";
import axios from "axios";
import Cookies from "js-cookie"


export default function Register(){
    const {
        history,
    } = useContext(AppContext);

    const [first, setFirst] = useState(false);

    useEffect(() => {
        console.log("effect register");
        if(!first){
            setFirst(true);
            if(Cookies.get('token') != undefined){
                window.location = '/';
            }
        }
    }, []);

    const onFinish = (values) => {
        console.log('Received values of form: ', values);
        if(values.password !== values.password_confirm){
            message.error("Password doesn't match!");
        }else{
            axios.post(`https://backendexample.sanbersy.com/api/register`,{
                name: values.name,
                email:values.email,
                password: values.password
            }).then((result) => {
                console.log(result.data);
                // Register success
                Cookies.set('token', result.data.token, { expires: 1 });
                Cookies.set('user', JSON.stringify(result.data.user), { expires: 1 });

                window.location = "/";
            }).catch((error) => {
                console.log(error.response.data);
                message.error(error.response.data)
            });
        }
    };

    return (
		<>
            <Row style={{padding:"10rem", margin:0}} gutter={16} >
                    <Card style={{ width: 400}}>
                        <Form name="register" initialValues={{ }} onFinish={onFinish}>
                            <div style={{textAlign:"center", marginBottom:"2rem"}}>
                                <h1>Register</h1>
                            </div>
                            <Form.Item name="name" rules={[{ required: true, message: 'Please input your name!' }]} >
                                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Name" />
                            </Form.Item>
                            <Form.Item name="email" rules={[{ required: true, message: 'Please input your email!' }]} >
                                <Input type="email" prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
                            </Form.Item>
                            <Form.Item name="password" rules={[{ required: true, message: 'Please input your Password!' }, {min:6, message:"The password must be at least 6 characters"}]} >
                                <Input prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="Password" />
                            </Form.Item>
                            <Form.Item name="password_confirm" rules={[{ required: true, message: 'Please input password confirmation!' }, {min:6, message:"The password must be at least 6 characters"}]} >
                                <Input prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="Password" />
                            </Form.Item>

                            <Form.Item>
                                <Button type="primary" htmlType="submit" style={{width:"100%"}}>
                                    Register
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </Row>
                <div style={{height:"160px"}}>
                </div>
		</>
    );
}