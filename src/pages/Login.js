import { Card, Col, Row, Form, Input, Button, Checkbox, message } from "antd";
import { UserOutlined, LockOutlined, MailOutlined } from '@ant-design/icons';

import React, { useContext, useEffect, useState } from "react";
import CardComponent from "../components/card";
import Section from "../components/section";
import { AppContext } from "../context/AppContext";
import { Link } from "react-router-dom";
import axios from "axios";
import Cookies from "js-cookie";

const { Meta } = Card;
export default function Login(){
    const {
        history,
    } = useContext(AppContext);

    const [first, setFirst] = useState(false);

    useEffect(() => {
        console.log("effect login");
        if(!first){
            setFirst(true);
            if(Cookies.get('token') != undefined){
                window.location = '/';
            }
        }
    }, []);

    const onFinish = (values) => {
        console.log('Received values of form: ', values);
        axios.post(`https://backendexample.sanbersy.com/api/user-login`,{
            email: values.email,
            password: values.password
        }).then((result) => {
            console.log(result.data);
            // Login success
            Cookies.set('token', result.data.token, { expires: 1 });
            Cookies.set('user', JSON.stringify(result.data.user), { expires: 1 });

            window.location = "/";
        }).catch((error) => {
            console.log("ERROR");
            console.log(error.response.data);
            message.error(String(error.response.data.error));
        });
    };

    return (
		<>
            <Row style={{padding:"10rem", margin:0}} gutter={16}>
                    <Card style={{ width: 400}}>
                        <Form name="login" initialValues={{ remember: true }} onFinish={onFinish} >
                            <div style={{textAlign:"center", marginBottom:"2rem"}}>
                                <h1>Login</h1>
                            </div>
                            <Form.Item name="email" rules={[{ required: true, message: 'Please input your Email!' }]} >
                                <Input type="email" prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email" />
                            </Form.Item>
                            <Form.Item name="password" rules={[{ required: true, message: 'Please input your Password!' }, {min:6, message:"The password must be at least 6 characters"}]} >
                                <Input prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="Password" />
                            </Form.Item>
                            <Form.Item>
                                <Form.Item name="remember" valuePropName="checked" noStyle>
                                    <Checkbox>Remember me</Checkbox>
                                </Form.Item>
                                <Link to="#" style={{float:"right"}} href="">Forgot password </Link>
                            </Form.Item>

                            <Form.Item>
                                <Button type="primary" htmlType="submit" style={{width:"100%"}}>
                                Log in
                                </Button>
                                Or <Link to="/register">register now!</Link>
                            </Form.Item>
                        </Form>
                    </Card>
                </Row>
                <div style={{height:"160px"}}>
                </div>
		</>
    );
}