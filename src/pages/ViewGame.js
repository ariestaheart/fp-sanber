import { Col, Row } from "antd";
import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import CardComponent from "../components/card";
import CardApp from "../components/cardApp";
import Footer from "../components/footer";
import Navbar from "../components/navbar";
import Section from "../components/section";
import { AppContext } from "../context/AppContext";
import axios from "axios";
import SideBar from "../components/sidebar";

export default function ViewGame(){
    const {
        history,
    } = useContext(AppContext);


    const [first, setFirst] = useState(false);
    const [data, setData] = useState({title:"test"});
    const {ID} = useParams();

    useEffect(() => {
        if(!first){
            console.log("effect view movie");
            setFirst(true);
            if(ID != undefined){
                console.log("first", first);
                axios.get(`https://backendexample.sanbersy.com/api/data-game/${ID}`).then((res) => {
                    setData(res.data);
                });
            }
        }
    }, []);

    return (
		<>
            <div style={{display:"flex"}}>
                <SideBar/>
                <Section>
                    <div className="card">
                        <h2>{data.name}</h2>
                        <div style={{display:"flex"}}>
                            <img className="fakeimg" src={data.image_url}/>
                            <div style={{marginLeft:"1rem"}}>
                                <p style={{marginBottom:0}}><strong>Genre: </strong>{data.genre}</p>
                                <p style={{marginBottom:0}}><strong>Platform: </strong>{data.platform} minutes</p>
                                <p style={{marginBottom:0}}><strong>Mode: </strong>{ data.multiplayer == 1 ? "Multiplayer" : "" } { data.multiplayer == 1 && data.singlePlayer == 1 ? "&" : "" } { data.singlePlayer == 1 ? "Single Player" : "" }</p>
                                <p style={{marginBottom:0}}><strong>Year: </strong>{data.release}</p>
                            </div>
                        </div>
                        <hr />
                    </div>
                </Section>
            </div>
		</>
    );
}