import { Col, Row } from "antd";
import React, { useContext, useEffect, useState } from "react";
import CardComponent from "../components/card";
import CardApp from "../components/cardApp";
import Footer from "../components/footer";
import Navbar from "../components/navbar";
import Section from "../components/section";
import SideBar from "../components/sidebar";
import { AppContext } from "../context/AppContext";

export default function GameListView(){
    const {
        history,
        movieList,
        gameList,
        getGameList,
        getMovieList,
        checkAuth
    } = useContext(AppContext);

    const [first, setFirst] = useState(false);

    useEffect(() => {
        console.log("effect home");
        if(!first){
            // checkAuth();
            getGameList();
            setFirst(true);    
        }
    }, []);

    return (
		<>
            <div style={{display:"flex"}}>
                <SideBar/>
                <Section>
                    <div style={{textAlign:"center", marginBottom:"2rem"}}>
                        <h1>Games List</h1>
                    </div>
                    <Row gutter={16}>
                        {gameList.map((data, index) => {
                            return(
                                <Col key={index} onClick={() => history.push(`/game/${data.id}`)} span={6} style={{marginBottom:"4rem"}}>
                                    <CardComponent data={data} type="game"></CardComponent>
                                </Col>
                            );
                        })}
                    </Row>
                </Section>
            </div>
		</>
    );
}