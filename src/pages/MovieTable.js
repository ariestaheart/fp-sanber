import React, { useContext, useEffect, useState } from "react";
import { AppContext } from "../context/AppContext";

import { Table, Button, notification, message, Input, Form } from "antd";
import { PlusSquareFilled, DeleteFilled, EditFilled } from '@ant-design/icons';
import { Link } from "react-router-dom";
import Section from "../components/section";
import axios from "axios";
import SideBar from "../components/sidebar";
import Cookies from "js-cookie";


export default function MovieTable(){
    const [form] = Form.useForm();

    const {
        history,
        movieList, setMovieList,
        getMovieList
    } = useContext(AppContext);

    const SORT = {
        NO : 1,
        TITLE : 2,
        GENRE : 3,
        DESCRIPTION : 4,
        YEAR : 5,
        DURATION : 6,
        RATING : 7,
        REVIEW : 8,
    }

    const ASC = 0;
    const DESC = 1;

    const [first, setFirst] = useState(false);
    const [sortActive, setSortActive] = useState(SORT.NO);
    const [sortType, setSortType] = useState({
        NO : ASC,
        TITLE : ASC,
        GENRE : ASC,
        DESCRIPTION : ASC,
        YEAR : ASC,
        DURATION : ASC,
        RATING : ASC,
        REVIEW : ASC,
    });

    let normalColumnName = {
        NO : "No",
        TITLE : "Title",
        GENRE : "Genre",
        DESCRIPTION : "Desc.",
        YEAR : "Year",
        DURATION : "Duration",
        RATING : "Rating",
        REVIEW : "Review",
    };

    const [columnName, setColumnName] = useState(normalColumnName);

    const [tableData, setTableData] = useState([]);

    const handleDelete = (event) => {
        let id = parseInt(event.currentTarget.value);
        axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${id}`,
        {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}).then((res) => {
            message.success('Data berhasil dihapus');
            getMovieList();
        }).catch((error) => {
            message.error('Data gagal dihapus');
        });
    }

    // Filter Manual
    const [filterRules, setFilterRules] = useState({
        title: undefined,
        genre: undefined,
        year: undefined,
    });

    const applyFilterRules = (values) => {
        console.log(values);
        setFilterRules({
            title: values.title,
            genre: values.genre,
            year: values.year
        });
    }

    const clearFilterRules = () => {
        setFilterRules({
            title: undefined,
            genre: undefined,
            year: undefined,
        });
        form.setFieldsValue({
            title: undefined,
            genre: undefined,
            year: undefined,
        });
    }

    useEffect(() => {
        if(!first){
            setFirst(true);
            getMovieList();
        }

        let temp_data = [...movieList];

        // Ini filter
        if(!(filterRules.title == undefined || filterRules.title == '')){
            temp_data = temp_data.filter((data) => 
                data.title.toLowerCase().includes(filterRules.title.toLowerCase())
            )
        }

        if(!(filterRules.genre == undefined || filterRules.genre == '')){
            temp_data = temp_data.filter((data) => 
                data.genre.toLowerCase().includes(filterRules.genre.toLowerCase())
            )
        }

        if(!(filterRules.year == undefined || filterRules.year == '')){
            temp_data = temp_data.filter((data) => 
                data.year == filterRules.year
            )
        }

        // Fungsi Sorting Manual
        if(sortActive == SORT.NO){
            let ascdesc = sortType.NO == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, NO: `No (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.NO == ASC){
                    return (a.number < b.number) ? -1 : 1;
                }
                return (a.number > b.number) ? -1 : 1;
            });
        }else if(sortActive == SORT.TITLE){
            let ascdesc = sortType.TITLE == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, TITLE: `Title (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.TITLE == ASC){
                    return (a.title.toLowerCase() < b.title.toLowerCase()) ? -1 : 1;
                }
                return (a.title.toLowerCase() > b.title.toLowerCase()) ? -1 : 1;
            });
        }else if(sortActive == SORT.GENRE){
            let ascdesc = sortType.GENRE == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, GENRE: `Genre (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.GENRE == ASC){
                    return (a.genre.toLowerCase() < b.genre.toLowerCase()) ? -1 : 1;
                }
                return (a.genre.toLowerCase() > b.genre.toLowerCase()) ? -1 : 1;
            });
        }else if(sortActive == SORT.DESCRIPTION){
            let ascdesc = sortType.DESCRIPTION == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, DESCRIPTION: `Desc. (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.DESCRIPTION == ASC){
                    return (a.description.toLowerCase() < b.description.toLowerCase()) ? -1 : 1;
                }
                return (a.description.toLowerCase() > b.description.toLowerCase()) ? -1 : 1;
            });
        }else if(sortActive == SORT.YEAR){
            let ascdesc = sortType.YEAR == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, YEAR: `Year (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.YEAR == ASC){
                    return (a.year < b.year) ? -1 : 1;
                }
                return (a.year > b.year) ? -1 : 1;
            });
        }else if(sortActive == SORT.DURATION){
            let ascdesc = sortType.DURATION == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, DURATION: `Duration (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.DURATION == ASC){
                    return (a.duration < b.duration) ? -1 : 1;
                }
                return (a.duration > b.duration) ? -1 : 1;
            });
        }else if(sortActive == SORT.RATING){
            let ascdesc = sortType.RATING == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, RATING: `Rating (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.RATING == ASC){
                    return (a.rating < b.rating) ? -1 : 1;
                }
                return (a.rating > b.rating) ? -1 : 1;
            });
        }else if(sortActive == SORT.REVIEW){
            let ascdesc = sortType.REVIEW == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, REVIEW: `Review (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.REVIEW == ASC){
                    return (a.review.toLowerCase() < b.review.toLowerCase()) ? -1 : 1;
                }
                return (a.review.toLowerCase() > b.review.toLowerCase()) ? -1 : 1;
            });
        }

        setTableData([...temp_data]);

        console.log("EFFECT RUN");
        console.log(tableData);
        console.log("SORT ACTIVE :: ", sortActive);
        console.log("SORT TYPE :: ", sortType);
    }, [getMovieList, sortActive, sortType, filterRules]);

    const columns = [
        {title:columnName.NO, dataIndex: 'number', key: 'number', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.NO);
                    setSortType({...sortType, NO: !sortType.NO});
                }
            };
        }, },
        {title:columnName.TITLE, dataIndex: 'title', key: 'title', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.TITLE);
                    setSortType({...sortType, TITLE: !sortType.TITLE});
                }
            };
        }},
        {title:columnName.GENRE, dataIndex: 'genre', key: 'genre', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.GENRE);
                    setSortType({...sortType, GENRE: !sortType.GENRE});
                }
            };
        }},
        {title:columnName.DESCRIPTION, dataIndex: 'description', key: 'description', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.DESCRIPTION);
                    setSortType({...sortType, DESCRIPTION: !sortType.DESCRIPTION});
                }
            };
        }},
        {title:columnName.YEAR, dataIndex: 'year', key: 'year', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.YEAR);
                    setSortType({...sortType, YEAR: !sortType.YEAR});
                }
            };
        }},
        {title:columnName.DURATION, dataIndex: 'duration', key: 'duration', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.DURATION);
                    setSortType({...sortType, DURATION: !sortType.DURATION});
                }
            };
        }},
        {title:columnName.RATING, dataIndex: 'rating', key: 'rating', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.RATING);
                    setSortType({...sortType, RATING: !sortType.RATING});
                }
            };
        }},
        {title:columnName.REVIEW, dataIndex: 'review', key: 'review', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.REVIEW);
                    setSortType({...sortType, REVIEW: !sortType.REVIEW});
                }
            };
        }},
        {title:'Action', key: 'aksi', fixed: 'right', width: 240, render: (data, index) => (
            <>
                <Link to={`/movie/edit/${data.id}`}>
                    <Button icon={<EditFilled />}>Edit</Button>
                </Link>
                <Button icon={<DeleteFilled />} danger onClick={handleDelete} value={data.id} style={{marginLeft:"1rem"}}>Delete</Button>
            </>
        )},
    ];

    return (
		<>
            <div style={{display:"flex"}}>
                <SideBar/>
                <Section>
                    <div style={{textAlign:"center"}}>
                        <h1>List Movie</h1>
                    </div>
                    <div style={{width:"100%", display:"flex", marginTop:"1rem"}}>
                        <Link to="/movie/add">
                            <Button icon={<PlusSquareFilled />} style={{marginBottom:"1rem"}}>Add new data</Button>
                        </Link>
                        <div style={{flexGrow:1}}></div>
                        <div style={{display:"flex", marginRight:"2rem"}}>
                            <strong style={{position:"relative", display:"inline-flex", alignItems:"center", maxWidth:"100%", height:"32px", marginLeft:"1rem"}}>
                                Flter:
                            </strong>
                            <Form name="basic" form={form}
                                    onFinish={applyFilterRules}
                                    labelCol={{ span: 10 }}
                                    wrapperCol={{ span: 14 }}
                                    initialValues={{ remember: true }}
                                    autoComplete="off" style={{width:"auto", padding:"0px", margin:"0px auto", display:"flex"}}>
                                <Form.Item name="title" label="Title" >
                                    <Input type="text" placeholder="" name="title" value="" />
                                </Form.Item>
                                <Form.Item name="genre" label="Genre" >
                                    <Input type="text" placeholder="" name="genre" value=""/>
                                </Form.Item>
                                <Form.Item name="year" label="Year">
                                    <Input type="number" min={1980} max={2021} placeholder="" name="year" value="1980"/>
                                </Form.Item>
                                <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
                                    <div style={{display:"flex"}}>
                                        <Button onClick={clearFilterRules} type="info" htmlType="button">Reset</Button>
                                        <Button type="primary" htmlType="submit">Apply</Button>
                                    </div>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                    <Table columns={columns} dataSource={tableData} />
                </Section>
            </div>
		</>
    );
}