import React, { useContext, useEffect, useState } from "react";
import { AppContext } from "../context/AppContext";

import { Table, Button, notification, message, Input, Form } from "antd";
import { PlusSquareFilled, DeleteFilled, EditFilled } from '@ant-design/icons';
import { Link } from "react-router-dom";
import Section from "../components/section";
import axios from "axios";
import SideBar from "../components/sidebar";
import Cookies from "js-cookie";


export default function GameTable(){
    const [form] = Form.useForm();

    const {
        history,
        gameList, setGameList,
        getGameList
    } = useContext(AppContext);

    // genre: string [OK]
    // image_url: string
    // singlePlayer: boolean (true or false) / (1 or 0)
    // multiplayer: boolean (true or false) / (1 or 0)
    // name: string
    // platform: string
    // release: string (minimal 2000 dan maksimal 2021)

    const SORT = {
        NO : 1,
        NAME : 2,
        GENRE : 3,
        SINGLE_PLAYER : 4,
        MULTI_PLAYER : 5,
        PLATFORM : 6,
        RELEASE : 7,
    }

    const ASC = 0;
    const DESC = 1;

    const [first, setFirst] = useState(false);
    const [sortActive, setSortActive] = useState(SORT.NO);
    const [sortType, setSortType] = useState({
        NO : ASC,
        NAME : ASC,
        GENRE : ASC,
        SINGLE_PLAYER : ASC,
        MULTI_PLAYER : ASC,
        PLATFORM : ASC,
        RELEASE : ASC,
    });

    let normalColumnName = {
        NO : "No",
        NAME : "Name",
        GENRE : "Genre",
        SINGLE_PLAYER : "Singgle Player",
        MULTI_PLAYER : "Multi Player",
        PLATFORM : "Platform",
        RELEASE : "Release",
    };

    const [columnName, setColumnName] = useState(normalColumnName);

    const [tableData, setTableData] = useState([]);

    const handleDelete = (event) => {
        let id = parseInt(event.currentTarget.value);
        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${id}`,
        {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}).then((res) => {
            message.success('Data berhasil dihapus');
            getGameList();
        }).catch((error) => {
            message.error('Data gagal dihapus');
        });
    }

    // Filter Manual
    const [filterRules, setFilterRules] = useState({
        name: undefined,
        genre: undefined,
        release: undefined,
    });

    const applyFilterRules = (values) => {
        console.log(values);
        setFilterRules({
            name: values.name,
            genre: values.genre,
            release: values.release
        });
    }

    const clearFilterRules = () => {
        setFilterRules({
            name: undefined,
            genre: undefined,
            release: undefined,
        });
        form.setFieldsValue({
            name: undefined,
            genre: undefined,
            release: undefined,
        });
    }

    useEffect(() => {
        if(!first){
            setFirst(true);
            getGameList();
        }

        let temp_data = [...gameList];

        // Ini filter
        if(!(filterRules.name == undefined || filterRules.name == '')){
            temp_data = temp_data.filter((data) => 
                data.name.toLowerCase().includes(filterRules.name.toLowerCase())
            )
        }

        if(!(filterRules.genre == undefined || filterRules.genre == '')){
            temp_data = temp_data.filter((data) => 
                data.genre.toLowerCase().includes(filterRules.genre.toLowerCase())
            )
        }

        if(!(filterRules.release == undefined || filterRules.release == '')){
            temp_data = temp_data.filter((data) => 
                data.release == filterRules.release
            )
        }

        // Fungsi Sorting Manual
        if(sortActive == SORT.NO){
            let ascdesc = sortType.NO == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, NO: `No (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.NO == ASC){
                    return (a.number < b.number) ? -1 : 1;
                }
                return (a.number > b.number) ? -1 : 1;
            });
        }else if(sortActive == SORT.NAME){
            let ascdesc = sortType.NAME == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, NAME: `Name (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.NAME == ASC){
                    return (a.name.toLowerCase() < b.name.toLowerCase()) ? -1 : 1;
                }
                return (a.name.toLowerCase() > b.name.toLowerCase()) ? -1 : 1;
            });
        }else if(sortActive == SORT.GENRE){
            let ascdesc = sortType.GENRE == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, GENRE: `Genre (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.GENRE == ASC){
                    return (a.genre.toLowerCase() < b.genre.toLowerCase()) ? -1 : 1;
                }
                return (a.genre.toLowerCase() > b.genre.toLowerCase()) ? -1 : 1;
            });
        }else if(sortActive == SORT.SINGLE_PLAYER){
            let ascdesc = sortType.SINGLE_PLAYER == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, SINGLE_PLAYER: `Single Player (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.SINGLE_PLAYER == ASC){
                    return (a.singlePlayer < b.singlePlayer) ? -1 : 1;
                }
                return (a.singlePlayer > b.singlePlayer) ? -1 : 1;
            });
        }else if(sortActive == SORT.MULTI_PLAYER){
            let ascdesc = sortType.MULTI_PLAYER == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, MULTI_PLAYER: `Multi Player (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.MULTI_PLAYER == ASC){
                    return (a.multiplayer < b.multiplayer) ? -1 : 1;
                }
                return (a.multiplayer > b.multiplayer) ? -1 : 1;
            });
        }else if(sortActive == SORT.PLATFORM){
            let ascdesc = sortType.PLATFORM == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, PLATFORM: `Platform (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.PLATFORM == ASC){
                    return (a.platform.toLowerCase() < b.platform.toLowerCase()) ? -1 : 1;
                }
                return (a.platform.toLowerCase() > b.platform.toLowerCase()) ? -1 : 1;
            });
        }else if(sortActive == SORT.RELEASE){
            let ascdesc = sortType.RELEASE == ASC ? "ASC" : "DESC";
            setColumnName({...normalColumnName, RELEASE: `Release Year (${ascdesc})`});
            temp_data.sort((a,b) => {
                if(sortType.RELEASE == ASC){
                    return (a.release < b.release) ? -1 : 1;
                }
                return (a.release > b.release) ? -1 : 1;
            });
        }

        setTableData([...temp_data]);

        console.log("EFFECT RUN");
        console.log(tableData);
        console.log("SORT ACTIVE :: ", sortActive);
        console.log("SORT TYPE :: ", sortType);
    }, [getGameList, sortActive, sortType, filterRules]);

    const columns = [
        {title:columnName.NO, dataIndex: 'number', key: 'number', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.NO);
                    setSortType({...sortType, NO: !sortType.NO});
                }
            };
        }, },
        {title:columnName.NAME, dataIndex: 'name', key: 'name', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.NAME);
                    setSortType({...sortType, NAME: !sortType.NAME});
                }
            };
        }},
        {title:columnName.GENRE, dataIndex: 'genre', key: 'genre', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.GENRE);
                    setSortType({...sortType, GENRE: !sortType.GENRE});
                }
            };
        }},
        {title:columnName.SINGLE_PLAYER, dataIndex: 'singlePlayer', key: 'singlePlayer', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.SINGLE_PLAYER);
                    setSortType({...sortType, SINGLE_PLAYER: !sortType.SINGLE_PLAYER});
                }
            };
        }},
        {title:columnName.MULTI_PLAYER, dataIndex: 'multiplayer', key: 'multiplayer', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.MULTI_PLAYER);
                    setSortType({...sortType, MULTI_PLAYER: !sortType.MULTI_PLAYER});
                }
            };
        }},
        {title:columnName.PLATFORM, dataIndex: 'platform', key: 'platform', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.PLATFORM);
                    setSortType({...sortType, PLATFORM: !sortType.PLATFORM});
                }
            };
        }},
        {title:columnName.RELEASE, dataIndex: 'release', key: 'release', onHeaderCell: (column) => {
            return {
                onClick: () => {
                    setSortActive(SORT.RELEASE);
                    setSortType({...sortType, RELEASE: !sortType.RELEASE});
                }
            };
        }},
        {title:'Action', key: 'aksi', fixed: 'right', width: 240, render: (data, index) => (
            <>
                <Link to={`/game/edit/${data.id}`}>
                    <Button icon={<EditFilled />}>Edit</Button>
                </Link>
                <Button icon={<DeleteFilled />} danger onClick={handleDelete} value={data.id} style={{marginLeft:"1rem"}}>Delete</Button>
            </>
        )},
    ];

    return (
		<>
            <div style={{display:"flex"}}>
                <SideBar/>
                <Section>
                    <div style={{textAlign:"center"}}>
                        <h1>List Game</h1>
                    </div>
                    <div style={{width:"100%", display:"flex", marginTop:"1rem"}}>
                        <Link to="/game/add">
                            <Button icon={<PlusSquareFilled />} style={{marginBottom:"1rem"}}>Add new data</Button>
                        </Link>
                        <div style={{flexGrow:1}}></div>
                        <div style={{display:"flex", marginRight:"2rem"}}>
                            <strong style={{position:"relative", display:"inline-flex", alignItems:"center", maxWidth:"100%", height:"32px", marginLeft:"1rem"}}>
                                Flter:
                            </strong>
                            <Form name="basic" form={form}
                                    onFinish={applyFilterRules}
                                    labelCol={{ span: 10 }}
                                    wrapperCol={{ span: 14 }}
                                    initialValues={{ remember: true }}
                                    autoComplete="off" style={{width:"auto", padding:"0px", margin:"0px auto", display:"flex"}}>
                                <Form.Item name="name" label="Name" >
                                    <Input type="text" placeholder="" name="name" value="" />
                                </Form.Item>
                                <Form.Item name="genre" label="Genre" >
                                    <Input type="text" placeholder="" name="genre" value=""/>
                                </Form.Item>
                                <Form.Item name="release" label="Release">
                                    <Input type="number" min={1980} max={2021} placeholder="" name="release" value="1980"/>
                                </Form.Item>
                                <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
                                    <div style={{display:"flex"}}>
                                        <Button onClick={clearFilterRules} type="info" htmlType="button">Reset</Button>
                                        <Button type="primary" htmlType="submit">Apply</Button>
                                    </div>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                    <Table columns={columns} dataSource={tableData} />
                </Section>
            </div>
		</>
    );
}