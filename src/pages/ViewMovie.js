import { Col, Row } from "antd";
import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import CardComponent from "../components/card";
import CardApp from "../components/cardApp";
import Footer from "../components/footer";
import Navbar from "../components/navbar";
import Section from "../components/section";
import { AppContext } from "../context/AppContext";
import axios from "axios";
import SideBar from "../components/sidebar";

export default function ViewMovie(){
    const {
        history,
    } = useContext(AppContext);


    const [first, setFirst] = useState(false);
    const [data, setData] = useState({title:"test"});
    const {ID} = useParams();

    useEffect(() => {
        if(!first){
            console.log("effect view movie");
            setFirst(true);
            if(ID != undefined){
                console.log("first", first);
                axios.get(`https://backendexample.sanbersy.com/api/data-movie/${ID}`).then((res) => {
                    setData(res.data);
                });
            }
        }
    }, []);

    return (
		<>
            <div style={{display:"flex"}}>
                <SideBar/>
                <Section>
                    <h2>{data.title}</h2>
                    <div style={{display:"flex"}}>
                        <img className="fakeimg" src={data.image_url}/>
                        <div style={{marginLeft:"1rem"}}>
                            <p style={{marginBottom:0}}><strong>Genre: </strong>{data.genre}</p>
                            <p style={{marginBottom:0}}><strong>Duration: </strong>{data.duration} minutes</p>
                            <p style={{marginBottom:0}}><strong>Rating: </strong>{data.rating} / 10</p>
                            <p style={{marginBottom:0}}><strong>Year: </strong>{data.year}</p>
                            <p style={{marginBottom:0}}>
                                <strong >Description : </strong>
                                {data.description}
                            </p>
                            <p style={{marginBottom:0}}>
                                <strong >Review : </strong>
                                {data.review}
                            </p>
                        </div>
                    </div>
                    <hr />
                </Section>
            </div>
		</>
    );
}