import { Col, Row, Layout, Menu, Breadcrumb, Button, Form, Input, Checkbox, message } from "antd";
import TextArea from "antd/lib/input/TextArea";

import React, { useContext, useEffect, useState } from "react";
import CardComponent from "../components/card";
import Section from "../components/section";
import { AppContext } from "../context/AppContext";
import { UserOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import Cookies from "js-cookie"
import SideBar from "../components/sidebar";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import axios from "axios";


const { SubMenu } = Menu;
const { Sider } = Layout;

export default function MovieForm(){
    const [form] = Form.useForm();

    const {
        history,
        inputMovie, setInputMovie,
        getMovieByID
    } = useContext(AppContext);

    const {ID} = useParams();

    const [first, setFirst] = useState(false);

    useEffect(() => {
        console.log("effect movie form");
        if(!first){
            setFirst(true);
            // get movie
            if(ID != undefined){
                getMovieByID(ID);
                console.log(ID);
            }else{
                setInputMovie({
                    created_at: "",
                    description: "",
                    duration: 0,
                    genre: "",
                    id: -1,
                    image_url: "",
                    rating: 0,
                    review: "",
                    title: "",
                    updated_at: "",
                    year: 1980,
                });
            }
        }
        form.setFieldsValue({...inputMovie});
    }, [getMovieByID]);

    const onFinish = (values) => {
        console.log(`Values :: `, values)
        if(ID != undefined){
            // put
            axios.put(`https://backendexample.sanbersy.com/api/data-movie/${ID}`,
                { ...values }, {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
            ).then((result) => {
                message.success('Data berhasil diedit');
                history.push('/movie/all');
            }).catch((error) => {
                message.error('Data gagal diedit');
            });
        }else{
            // post
            axios.post(`https://backendexample.sanbersy.com/api/data-movie`,
                { ...values }, {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
            ).then((result) => {
                message.success('Data berhasil ditambahkan');
                history.push('/movie/all');
            }).catch((error) => {
                message.error('Data gagal ditambahkan');
            });
        }
    }

    return (
		<>
            <div style={{display:"flex"}}>
                <SideBar/>
                <Section>
                    <div style={{textAlign:"center"}}>
                        <h1>Form Movie</h1>
                    </div>
                    <div style={{width:"100%"}}>
                        <Form  name="basic" form={form}
                                onFinish={onFinish}
                                labelCol={{ span: 4 }}
                                wrapperCol={{ span: 20 }}
                                initialValues={{ remember: true }}
                                autoComplete="off" style={{width:"75%", padding:"0px", margin:"0px auto"}}>
                            <Form.Item name="title" label="Title" rules={[{ required: true, message: `Title is required!` }]}>
                                <Input type="text" placeholder="" name="title" value="" />
                            </Form.Item>
                            <Form.Item name="genre" label="Genre" rules={[{ required: true, message: `Genre is required!` }]} >
                                <Input type="text" placeholder="" name="genre" value=""/>
                            </Form.Item>
                            <Form.Item name="description" label="Description" rules={[{ required: true, message: `Description is required!` }]} >
                                <TextArea placeholder="" name="description" value=""/>
                            </Form.Item>
                            <Form.Item name="year" label="Release Year" rules={[{ required: true, message: `Release Year is required!` }]}>
                                <Input type="number" min={1980} max={2021} placeholder="" name="year" value="1980"/>
                            </Form.Item>
                            <Form.Item name="duration" label="Duration (minutes)" rules={[{ required: true, message: `Movie duration is required!` }]}>
                                <Input type="number" placeholder="" name="duration" value="0"/>
                            </Form.Item>
                            <Form.Item name="rating" label="Rating" rules={[{ required: true, message: `Rating is required!` }]}>
                                <Input type="number" min={0} max={10} placeholder="" name="rating" value="0"/>
                            </Form.Item>
                            <Form.Item name="image_url" label="Image Url" rules={[{ required: true, message: `Image is required!` }]}>
                                <Input type="text" placeholder="" name="image_url" value=""/>
                            </Form.Item>
                            <Form.Item name="review" label="Review" rules={[{ required: true, message: `Review is required!` }]} >
                                <TextArea placeholder="" name="review" value=""/>
                            </Form.Item>

                            <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
                                <Button type="primary" htmlType="submit">
                                Submit
                                </Button>
                            </Form.Item>
                        </Form>

                    </div>
                </Section>
            </div>
		</>
    );
}