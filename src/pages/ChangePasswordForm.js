import { Layout, Menu, Button, Form, Input, message } from "antd";
import TextArea from "antd/lib/input/TextArea";

import React, { useContext, useEffect, useState } from "react";
import Section from "../components/section";
import { AppContext } from "../context/AppContext";
import Cookies from "js-cookie"
import SideBar from "../components/sidebar";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import axios from "axios";
import Checkbox from "antd/lib/checkbox/Checkbox";

export default function ChangePasswordForm(){
    const [form] = Form.useForm();

    const {
        history,
    } = useContext(AppContext);

    const [first, setFirst] = useState(false);

    useEffect(() => {
        console.log("effect reset password");
        if(!first){
            setFirst(true);
        }
    }, []);

    const onFinish = (values) => {
        console.log(`Values :: `, values)
        // return;
        // post
        axios.post(`https://backendexample.sanbersy.com/api/change-password`,
            { ...values }, {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
        ).then((result) => {
            message.success('Password berhasil di ganti');
        }).catch((error) => {
            let response = JSON.parse(error.response.data);
            if(response.current_password){
                message.error(response.current_password);
            }
            if(response.new_confirm_password){
                message.error(response.new_confirm_password);
            }
        });
    }

    return (
		<>
            <div style={{display:"flex"}}>
                <SideBar/>
                <Section>
                    <div style={{textAlign:"center"}}>
                        <h1>Change Password</h1>
                    </div>
                    <div style={{width:"100%"}}>
                        <Form  name="basic" form={form}
                                onFinish={onFinish}
                                labelCol={{ span: 4 }}
                                wrapperCol={{ span: 20 }}
                                initialValues={{ remember: true }}
                                autoComplete="off" style={{width:"75%", padding:"0px", margin:"0px auto"}}>
                            <Form.Item name="current_password" label="Current password" rules={[{ required: true, message: `Current password is required!` }, {min:6, message:"Current password must be at least 6 characters"}]}>
                                <Input type="password" placeholder="" name="current_password" value="" />
                            </Form.Item>
                            <Form.Item name="new_password" label="New Password" rules={[{ required: true, message: `New Password is required!` }, {min:6, message:"New Password must be at least 6 characters"}]}>
                                <Input type="password" placeholder="" name="new_password" value="" />
                            </Form.Item>
                            <Form.Item name="new_confirm_password" label="New Confirm Password" rules={[{ required: true, message: `New Confirm Password is required!` }, {min:6, message:"New Confirm Password must be at least 6 characters"}]}>
                                <Input type="password" placeholder="" name="new_confirm_password" value="" />
                            </Form.Item>


                            <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
                                <Button type="primary" htmlType="submit">
                                Submit
                                </Button>
                            </Form.Item>
                        </Form>

                    </div>
                </Section>
            </div>
		</>
    );
}