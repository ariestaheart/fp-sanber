import { Col, Row } from "antd";
import React, { useContext, useEffect, useState } from "react";
import CardComponent from "../components/card";
import CardApp from "../components/cardApp";
import Footer from "../components/footer";
import Navbar from "../components/navbar";
import Section from "../components/section";
import SideBar from "../components/sidebar";
import { AppContext } from "../context/AppContext";

export default function MovieListView(){
    const {
        history,
        movieList,
        gameList,
        getGameList,
        getMovieList
    } = useContext(AppContext);

    const [first, setFirst] = useState(false);

    useEffect(() => {
        console.log("effect home");
        if(!first){
            getMovieList();
            setFirst(true);    
        }
    }, []);

    return (
		<>
            <div style={{display:"flex"}}>
                <SideBar/>
                <Section>
                    <div style={{textAlign:"center", marginBottom:"2rem"}}>
                        <h1>Movies List</h1>
                    </div>
                    <Row gutter={16}>
                        {movieList.map((data, index) => {
                            return(
                                <Col key={index} onClick={() => history.push(`/movie/${data.id}`)} span={6} style={{marginBottom:"4rem"}}>
                                    <CardComponent data={data} type="movie"></CardComponent>
                                </Col>
                            );
                        })}
                    </Row>
                </Section>
            </div>
		</>
    );
}