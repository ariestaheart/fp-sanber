import { Layout, Menu, Button, Form, Input, message } from "antd";
import TextArea from "antd/lib/input/TextArea";

import React, { useContext, useEffect, useState } from "react";
import Section from "../components/section";
import { AppContext } from "../context/AppContext";
import Cookies from "js-cookie"
import SideBar from "../components/sidebar";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import axios from "axios";
import Checkbox from "antd/lib/checkbox/Checkbox";

export default function GameForm(){
    const [form] = Form.useForm();

    const {
        history,
        inputGame, setInputGame,
        getGameByID
    } = useContext(AppContext);

    const {ID} = useParams();

    const [first, setFirst] = useState(false);

    useEffect(() => {
        console.log("effect movie form");
        if(!first){
            setFirst(true);
            // get movie
            if(ID != undefined){
                getGameByID(ID);
                console.log(ID);
            }else{
                setInputGame({
                    created_at: "",
                    genre: "",
                    id: -1,
                    image_url: "",
                    multiplayer: 1,
                    singlePlayer: 1,
                    name: "",
                    platform: "",
                    release: 2021,
                    updated_at: "",
                });
            }
        }
        form.setFieldsValue({...inputGame});
    }, [inputGame, getGameByID]);

    const onFinish = (values) => {
        console.log(`Values :: `, values)
        // return;
        if(ID != undefined){
            // put
            axios.put(`https://backendexample.sanbersy.com/api/data-game/${ID}`,
                { ...values }, {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
            ).then((result) => {
                message.success('Data berhasil diedit');
                history.push('/game/all');
            }).catch((error) => {
                message.error('Data gagal diedit');
            });
        }else{
            // post
            axios.post(`https://backendexample.sanbersy.com/api/data-game`,
                { ...values }, {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}}
            ).then((result) => {
                message.success('Data berhasil ditambahkan');
                history.push('/game/all');
            }).catch((error) => {
                message.error('Data gagal ditambahkan');
            });
        }
    }

    return (
		<>
            <div style={{display:"flex"}}>
                <SideBar/>
                <Section>
                    <div style={{textAlign:"center"}}>
                        <h1>Form Game</h1>
                    </div>
                    <div style={{width:"100%"}}>
                        <Form  name="basic" form={form}
                                onFinish={onFinish}
                                labelCol={{ span: 4 }}
                                wrapperCol={{ span: 20 }}
                                initialValues={{ remember: true }}
                                autoComplete="off" style={{width:"75%", padding:"0px", margin:"0px auto"}}>
                            <Form.Item name="name" label="Name" rules={[{ required: true, message: `Name is required!` }]}>
                                <Input type="text" placeholder="" name="name" value="" />
                            </Form.Item>
                            <Form.Item name="genre" label="Genre" rules={[{ required: true, message: `Genre is required!` }]} >
                                <Input type="text" placeholder="" name="genre" value=""/>
                            </Form.Item>
                            <Form.Item name="release" label="Release Year" rules={[{ required: true, message: `Release Year is required!` }]}>
                                <Input type="number" min={1980} max={2021} placeholder="" name="release" value="1980"/>
                            </Form.Item>
                            <Form.Item name="image_url" label="Image Url" rules={[{ required: true, message: `Image is required!` }]}>
                                <Input type="text" placeholder="" name="image_url" value=""/>
                            </Form.Item>
                            <Form.Item name="platform" label="Platform" rules={[{ required: true, message: `Platform is required!` }]} >
                                <TextArea placeholder="" name="platform" value=""/>
                            </Form.Item>

                            <Form.Item name="singlePlayer" label="Single Player" valuePropName="checked" wrapperCol={{ offset: 0, span: 24 }}>
                                <Checkbox>Yes</Checkbox>
                            </Form.Item>
                            <Form.Item name="multiplayer" label="Multi Player" valuePropName="checked" wrapperCol={{ offset: 0, span: 24 }}>
                                <Checkbox>Yes</Checkbox>
                            </Form.Item>

                            <Form.Item wrapperCol={{ offset: 4, span: 20 }}>
                                <Button type="primary" htmlType="submit">
                                Submit
                                </Button>
                            </Form.Item>
                        </Form>

                    </div>
                </Section>
            </div>
		</>
    );
}