import React from "react";
import { Card } from "antd";
const { Meta } = Card;

export default function CardComponent(props){
    let data = props.data;

    return (
        <>
            <Card
                hoverable
                style={{ width: 280}}
                cover={<img alt="example" style={{height:240, objectFit:"cover"}} src={data.image_url} />}
            >
                {
                    (() => {
                       if(props.type === "game"){
                            return (
                                <>
                                    <Meta title={data.name}/>
                                </>
                            )
                        }else{
                            return (
                                <>
                                    <Meta title={data.title}/>
                                </>
                            )                        
                        }
                    })()
                }
                
                <hr/>
                {
                    (() => {
                       if(props.type === "game"){
                            return (
                                <>
                                    <p><strong>Release Year:</strong> {data.release}</p>
                                    <p><strong>Genre:</strong> {data.genre}</p>
                                    <p><strong>Platform:</strong> {data.platform}</p>
                                </>
                            )
                        }else{
                            return (
                                <>
                                    <p><strong>Release Year:</strong> {data.year}</p>
                                    <p><strong>Genre:</strong> {data.genre}</p>
                                    <p><strong>Rating:</strong> {data.rating} / 10</p>
                                </>
                            )                        }
                    })()
                }
            </Card>
        </>
    );
}