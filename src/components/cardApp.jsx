import React from "react";

export default function CardApp(props){
    let data = props.data;
    return (
        <>
            <div className="card">
                <h2>{data.name} ({data.release_year})</h2>
                <div style={{display:"flex"}}>
                    <img className="fakeimg" src={data.image_url}/>
                    <div style={{marginLeft:"1rem"}}>
                        <strong>Category: {data.category}</strong><br />
                        <strong>Price: {data.price === 0 ? "Free" : data.price}</strong><br />
                        <strong>Rating: {data.rating}</strong><br />
                        <strong>Size: {data.size} MB</strong><br />
                        <strong>Release Year: {data.release_year}</strong><br />
                        <strong style={{marginRight:"10px"}}>Platform : {data.platform}
                        </strong>
                        <br />
                        <p>
                            <strong style={{marginRight:"10px"}}>Description :</strong>
                            {data.description}
                        </p>
                    </div>
                </div>
                <hr />
            </div>
        </>
    );
}