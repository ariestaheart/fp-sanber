import React from "react";
import Logo from "../assets/img/logo.png";
import { Button, Form, Input } from "antd";

export default function Section(props){
    return (
        <>
            <div className="row" style={{margin:"1rem 4rem", width:"100%"}}>
                <div className="section">
                    {props.children}
                </div>
            </div>
        </>
    );
}