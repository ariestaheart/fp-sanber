import React, { useContext, useEffect } from "react";
import Logo from "../assets/img/logo.png";
import { Button, Form, Input, message } from "antd";
import { AppContext } from "../context/AppContext";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";

export default function Navbar() {
    
    const {
        user,
        message,
        history,
    } = useContext(AppContext);

    useEffect(() => {

    }, []);

    return (
    <>
        <div className="topnav">
            <div className="leftnav">
                {/* <a href="#">
                    <img src={Logo} width="70" />
                </a> */}
                <Link to="/">Home</Link>
                <Link to="/movies">Movie List</Link>
                <Link to="/games">Game List</Link>
            </div>
            <div className="rightnav">
                { (() => {
                    if(Cookies.get('token') != undefined){
                        return (
                        <>
                            Hai, { Cookies.get('token') && Cookies.get('user') != undefined ? JSON.parse(Cookies.get('user')).name : "" }
                        </>
                        );
                    }else {
                        return (<>
                            <Link to="/login">Login</Link>
                            <Link to="/register">Register</Link>
                        </>);
                    }
                })() }
            </div>
        </div>
    </>
    );
}