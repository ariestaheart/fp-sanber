import React from "react";
import Logo from "../assets/img/logo.png";
import { Button, Form, Input } from "antd";

export default function Body(props){
    return (
        <>
            <div className="row">
                <div className="section">
                    {props.children}
                </div>
            </div>
        </>
    );
}