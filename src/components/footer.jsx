import React from "react";

export default function Footer(props){
    return (
        <>
            <footer>
                <h5>Made with ❤ by Ariesta | ReactJS Sanbercode</h5>
            </footer>
        </>
    );
}