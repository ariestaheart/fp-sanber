import { Layout, Menu } from "antd";
import React, { useContext, useEffect, useState } from "react";

import { AppContext } from "../context/AppContext";
import { UserOutlined, LaptopOutlined } from '@ant-design/icons';
import Cookies from "js-cookie"

const { SubMenu } = Menu;
const { Sider } = Layout;

export default function SideBar(){
    const { history } = useContext(AppContext);

    const [first, setFirst] = useState(false);

    useEffect(() => {
        console.log("effect home");
        if(!first){
            setFirst(true);    
        }
    }, []);

    return (
        <Sider style={{display: Cookies.get('token') != undefined ? "block" : "none"}} width={200} className="site-layout-background">
            <Menu
            mode="inline"
            style={{ height: '100%', borderRight: 0 }}
            >
            <SubMenu key="sub1" icon={<UserOutlined />} title={ Cookies.get('token') && Cookies.get('user') != undefined ? "Halo, " + JSON.parse(Cookies.get('user')).name : "" }>
                <Menu.Item key="1" onClick={ () => { history.push('/changepassword'); }}>Ganti password</Menu.Item>
                <Menu.Item key="2" onClick={() => { Cookies.remove('token'); window.location = "/"; }}>Logout</Menu.Item>
            </SubMenu>
            <SubMenu key="sub2" icon={<LaptopOutlined />} title="Tools">
                <Menu.Item key="3" onClick={ () => { history.push('/movie/all'); }}>Manage Daftar Movie</Menu.Item>
                <Menu.Item key="4" onClick={ () => { history.push('/game/all'); }}>Manage Daftar Game</Menu.Item>
            </SubMenu>
            </Menu>
        </Sider>
    );
}